from setuptools import setup

setup(
    name='checknotes',
    version='0.0.2',
    py_modules=['checknotes'],
    entry_points={
        'console_scripts': [
            'checknotes = checknotes:run'
        ],
    },
)
