import re
import sys

from argparse import ArgumentParser
from collections import defaultdict
from fnmatch import fnmatch
from os import path, walk

sys.stdout = open(1, mode='w', encoding='utf-8')


def error(filename, line_and_column, message):
    l, c = line_and_column
    print('{} ({}:{}): {}'.format(filename, l, c, message))


def line_and_column_from_position(text, position):
    return (text.count('\n', 0, position) + 1,
            position - text.rfind('\n', 0, position))


def strip_numbering(code):
    lines = code.split('\n')
    return '\n'.join(re.sub(r'^\s*[0-9]+\. ?', '', line, re.MULTILINE)
                     for line in lines)


def get_codes(function_pattern, text):
    pattern = r'```pseudo\n(\s*(\d+\.)?\s*{}\(.*?)```'.format(function_pattern)
    function = re.compile(pattern, re.DOTALL)
    return [(match.group(1),
             line_and_column_from_position(text, match.start(1)))
            for match in function.finditer(text)]


def get_functions(text):
    return [match.group(2)
            for match in re.finditer(r'```pseudo\n(\s*\d+\.)?\s*([a-zA-Z0-9-]+)\(',
                                     text)]


def get_all_functions(files):
    return sorted(set(function for _, text in files
                      for function in get_functions(text)))


def different_occurrences(index):
    occurrences = []

    if len(index) > 1:
        occurrences += index.values()

    return occurrences


def read_file(filename):
    return open(filename, encoding='utf-8').read()


def build_index(function_pattern, files):
    codes = [(code, filename, index) for filename, text in files
             for code, index in
             get_codes(function_pattern, text)]

    instances = defaultdict(list)

    for code, filename, index in codes:
        instances[code].append((filename, index))
    return instances


def find(top, pattern):
    found = []
    for root, dirs, files in walk(top):
        found.extend(path.join(root, filename) for filename in files
                     if fnmatch(filename, pattern))
    return found


def check_style(files, main_file=None):
    blacklist = ((r'\[[a-zA-Z0-9]*\.\.[a-zA-Z0-9]*\]', 'Wrong slice syntax'),)

    for filename, text in files:
        for pattern, message in blacklist:
            matches = re.finditer(pattern, text)
            for match in matches:
                error(filename,
                      line_and_column_from_position(text, match.start()),
                      message)


def stripped(index):
    stripped_index = defaultdict(list)

    for code, info in index.items():
        stripped_index[strip_numbering(code)].extend(info)

    return stripped_index


def check_different(function, index, main_file=None):
    if main_file is not None:
        is_main_file = lambda x: x == main_file
    else:
        is_main_file = lambda x: True

    stripped_index = stripped(index)
    occurrences = different_occurrences(stripped_index)
    if occurrences:
        for line, occurrence in enumerate(sorted(occurrences), 1):
            for filename, line_and_column in sorted(occurrence):
                if is_main_file(filename):
                    error(filename, line_and_column,
                          "Version of '{}' differs from others."
                          .format(function))


def get_enumerations(code):
    return re.findall('^\s*(\d+)\.', code, re.MULTILINE)


def well_enumerated(code):
    lines = code.count('\n') + 1
    enumerations = get_enumerations(code)
    base = [str(i) for i in range(1, lines)]
    is_enumeration = enumerations == base
    is_all_empty = enumerations == []*lines
    return is_enumeration or is_all_empty


def check_enumerations(function, index):
    for code, occurrence in index.items():
        if not well_enumerated(code):
            for filename, line_and_column in occurrence:
                error(filename, line_and_column, 'Not well enumerated')


def check_indentation(index):
    for code, occurrence in index.items():
        lines = strip_numbering(code).split('\n')
        for n, line in enumerate(lines):
            whitespace = re.match('\s*', line).group()
            if len(whitespace) % 4 != 0:
                for filename, line_and_column in occurrence:
                    l, c = line_and_column
                    error(filename, (l + n, c), 'Bad indentation')


def check_files(all_files, main_file=None):
    if main_file is not None:
        files = [(f, c) for f, c in all_files if f == main_file]
    else:
        files = all_files

    functions = get_all_functions(all_files)
    for function in functions:
        all_index = build_index(function, all_files)
        index = build_index(function, files)
        check_different(function, all_index, main_file)
        check_enumerations(function, index)
        check_indentation(index)


def main(argv):
    parser = ArgumentParser()

    parser.add_argument('--project', action='store_true')
    parser.add_argument('filename')

    args = parser.parse_args(argv)

    dirpath = path.abspath(path.dirname(args.filename))
    fullpath = path.abspath(args.filename)

    if args.project:
        current = dirpath
        nextdir, _ = path.split(current)
        while current != nextdir:
            if path.exists(path.join(current, '.classproj')):
                dirpath = current
                break
            current = nextdir
            nextdir, _ = path.split(current)

    filenames = find(dirpath, '*.md')
    files = [(filename, read_file(filename)) for filename in filenames]

    if path.isfile(args.filename):
        main_file = path.join(dirpath, fullpath)
    else:
        main_file = None

    check_files(files, main_file)
    check_style(files, main_file)


def run():
    main(sys.argv[1:])


if __name__ == '__main__':
    run()
